//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Win32SimpleSample.rc
//
#define IDR_MENU1                       101
#define IDR_TOOLBAR1                    102
#define ID_INDICATOR_TOOL               103
#define IDD_DIALOG1                     109
#define IDD_DIALOGCAMERA                120
#define IDC_LIST1                       1001
#define IDC_CAMERALIST                  1003
#define IDC_SELECTCAMERA                1005
#define IDC_ADDCAMERA                   1006
#define IDC_DELETECAMERA                1007
#define IDC_POSITIONX                   1021
#define IDC_POSITIONLABEL               1022
#define IDC_CAMERASLABEL                1023
#define IDC_SELECTEDCAMERASETTINGGROUP  1024
#define IDC_SETTRANSFORM                1025
#define IDC_CAMERAFRONT                 1026
#define IDC_POSITIONY                   1027
#define IDC_POSITIONZ                   1028
#define IDC_ROTATIONLABEL               1029
#define IDC_ROTATIONX                   1030
#define IDC_ROTATIONY                   1031
#define IDC_ROTATIONZ                   1032
#define IDC_CAMERABACK                  1033
#define IDC_CAMERATOP                   1034
#define IDC_CAMERABOTTOM                1035
#define IDC_CAMERALEFT                  1036
#define IDC_CAMERARIGHT                 1037
#define ID_BUTTON40001                  40001
#define ID_FILE                         40002
#define ID_EDIT_SELECT                  40003
#define ID_FILE_QUIT                    40004
#define ID_FILE_SAVETERRAIN             40005
#define ID_TOOLS_CAMRA                  40012
#define ID_VIEW_CAMERATOOLBAR           40013
#define ID_VIEW_P                       40014
#define ID_VIEW_MAPGENERATOR            40015
#define ID_TOOLS_MAPGENERATOR           40016
#define ID_HELP_FEATURES                40017
#define ID_HELP_CREDITS                 40018
#define ID_HELP_ABOUT                   40019
#define ID_BUTTON2                      40020
#define ID_VIEW_SCENECAMERAS            40023

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        124
#define _APS_NEXT_COMMAND_VALUE         40024
#define _APS_NEXT_CONTROL_VALUE         1027
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif
