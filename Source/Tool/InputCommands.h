#pragma once

struct InputCommands {
	bool forward;
	bool back;
	bool right;
	bool left;
	bool up;
	bool down;
	bool rotateRight;
	bool rotateLeft;
	bool rotateUp;
	bool rotateDown;
};
