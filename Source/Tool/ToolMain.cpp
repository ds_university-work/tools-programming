#include "ToolMain.h"
#include "../Resource Files/resource.h"
#include <vector>
#include <sstream>

//ToolMain Class
ToolMain::ToolMain() {

	m_currentChunk = 0;		//default value
	m_selectedObject = 0;	//initial selection ID
	m_databaseConnection = NULL;
}


ToolMain::~ToolMain() {
	sqlite3_close(m_databaseConnection);		//close the database connection
}


int ToolMain::getCurrentSelectionID() {

	return m_selectedObject;
}

void ToolMain::onActionInitialise(HWND handle, int width, int height) {
	//window size, handle etc for directX
	m_width = width;
	m_height = height;
	m_d3dRenderer.GetDefaultSize(m_width, m_height);
	m_d3dRenderer.Initialize(handle, m_width, m_height, &m_input);

	//database connection establish
	int rc;
	rc = sqlite3_open("database/test.db", &m_databaseConnection);

	if (rc) {
		TRACE("Can't open database");
		//if the database cant open. Perhaps a more catastrophic error would be better here
	} else {
		TRACE("Opened database successfully");
	}

	onActionLoad();
}

void ToolMain::onActionLoad() {
	//load current chunk and objects into lists

	m_scene = Scene();

	//SQL
	int rc;
	char *sqlCommand;
	char *ErrMSG = 0;
	sqlite3_stmt *pResults;								//results of the query
	sqlite3_stmt *pResultsChunk;

	//OBJECTS IN THE WORLD
	//prepare SQL Text
	sqlCommand = "SELECT * from Objects";				//sql command which will return all records from the objects table.
	//Send Command and fill result object
	rc = sqlite3_prepare_v2(m_databaseConnection, sqlCommand, -1, &pResults, 0);

	//loop for each row in results until there are no more rows.  ie for every row in the results. We create and object
	while (sqlite3_step(pResults) == SQLITE_ROW) {
		SceneObject newSceneObject;
		newSceneObject.ID = sqlite3_column_int(pResults, 0);
		newSceneObject.chunk_ID = sqlite3_column_int(pResults, 1);
		newSceneObject.model_path = reinterpret_cast<const char*>(sqlite3_column_text(pResults, 2));
		newSceneObject.tex_diffuse_path = reinterpret_cast<const char*>(sqlite3_column_text(pResults, 3));
		newSceneObject.posX = sqlite3_column_double(pResults, 4);
		newSceneObject.posY = sqlite3_column_double(pResults, 5);
		newSceneObject.posZ = sqlite3_column_double(pResults, 6);
		newSceneObject.rotX = sqlite3_column_double(pResults, 7);
		newSceneObject.rotY = sqlite3_column_double(pResults, 8);
		newSceneObject.rotZ = sqlite3_column_double(pResults, 9);
		newSceneObject.scaX = sqlite3_column_double(pResults, 10);
		newSceneObject.scaY = sqlite3_column_double(pResults, 11);
		newSceneObject.scaZ = sqlite3_column_double(pResults, 12);
		newSceneObject.render = sqlite3_column_int(pResults, 13);
		newSceneObject.collision = sqlite3_column_int(pResults, 14);
		newSceneObject.collision_mesh = reinterpret_cast<const char*>(sqlite3_column_text(pResults, 15));
		newSceneObject.collectable = sqlite3_column_int(pResults, 16);
		newSceneObject.destructable = sqlite3_column_int(pResults, 17);
		newSceneObject.health_amount = sqlite3_column_int(pResults, 18);
		newSceneObject.editor_render = sqlite3_column_int(pResults, 19);
		newSceneObject.editor_texture_vis = sqlite3_column_int(pResults, 20);
		newSceneObject.editor_normals_vis = sqlite3_column_int(pResults, 21);
		newSceneObject.editor_collision_vis = sqlite3_column_int(pResults, 22);
		newSceneObject.editor_pivot_vis = sqlite3_column_int(pResults, 23);
		newSceneObject.pivotX = sqlite3_column_double(pResults, 24);
		newSceneObject.pivotY = sqlite3_column_double(pResults, 25);
		newSceneObject.pivotZ = sqlite3_column_double(pResults, 26);
		newSceneObject.snapToGround = sqlite3_column_int(pResults, 27);
		newSceneObject.AINode = sqlite3_column_int(pResults, 28);
		newSceneObject.audio_path = reinterpret_cast<const char*>(sqlite3_column_text(pResults, 29));
		newSceneObject.volume = sqlite3_column_double(pResults, 30);
		newSceneObject.pitch = sqlite3_column_double(pResults, 31);
		newSceneObject.pan = sqlite3_column_int(pResults, 32);
		newSceneObject.one_shot = sqlite3_column_int(pResults, 33);
		newSceneObject.play_on_init = sqlite3_column_int(pResults, 34);
		newSceneObject.play_in_editor = sqlite3_column_int(pResults, 35);
		newSceneObject.min_dist = sqlite3_column_double(pResults, 36);
		newSceneObject.max_dist = sqlite3_column_double(pResults, 37);
		newSceneObject.camera = sqlite3_column_int(pResults, 38);
		newSceneObject.path_node = sqlite3_column_int(pResults, 39);
		newSceneObject.path_node_start = sqlite3_column_int(pResults, 40);
		newSceneObject.path_node_end = sqlite3_column_int(pResults, 41);
		newSceneObject.parent_id = sqlite3_column_int(pResults, 42);
		newSceneObject.editor_wireframe = sqlite3_column_int(pResults, 43);
		newSceneObject.name = reinterpret_cast<const char*>(sqlite3_column_text(pResults, 44));

		// send completed object to scenegraph
		m_scene.m_sceneObjects.push_back(newSceneObject);
	}

	// Check if the scene has a camera object.
	bool cameraExists = false;
	for each (SceneObject sc in m_scene.m_sceneObjects) {
		if (sc.camera) {
			cameraExists = true;
			break;
		}
	}

	if (!cameraExists) {
		SceneObject* so = m_scene.AddNewSceneObject("Main Camera", "", "");
		so->camera = true;
		so->render = false;
	}

	//THE WORLD CHUNK
	//prepare SQL Text
	sqlCommand = "SELECT * from Chunks";				//sql command which will return all records from chunks table. There is only one though.
														//Send Command and fill result object
	rc = sqlite3_prepare_v2(m_databaseConnection, sqlCommand, -1, &pResultsChunk, 0);


	sqlite3_step(pResultsChunk);
	m_scene.m_chunk.ID = sqlite3_column_int(pResultsChunk, 0);
	m_scene.m_chunk.name = reinterpret_cast<const char*>(sqlite3_column_text(pResultsChunk, 1));
	m_scene.m_chunk.chunk_x_size_metres = sqlite3_column_int(pResultsChunk, 2);
	m_scene.m_chunk.chunk_y_size_metres = sqlite3_column_int(pResultsChunk, 3);
	m_scene.m_chunk.chunk_base_resolution = sqlite3_column_int(pResultsChunk, 4);
	m_scene.m_chunk.heightmap_path = reinterpret_cast<const char*>(sqlite3_column_text(pResultsChunk, 5));
	m_scene.m_chunk.tex_diffuse_path = reinterpret_cast<const char*>(sqlite3_column_text(pResultsChunk, 6));
	m_scene.m_chunk.tex_splat_alpha_path = reinterpret_cast<const char*>(sqlite3_column_text(pResultsChunk, 7));
	m_scene.m_chunk.tex_splat_1_path = reinterpret_cast<const char*>(sqlite3_column_text(pResultsChunk, 8));
	m_scene.m_chunk.tex_splat_2_path = reinterpret_cast<const char*>(sqlite3_column_text(pResultsChunk, 9));
	m_scene.m_chunk.tex_splat_3_path = reinterpret_cast<const char*>(sqlite3_column_text(pResultsChunk, 10));
	m_scene.m_chunk.tex_splat_4_path = reinterpret_cast<const char*>(sqlite3_column_text(pResultsChunk, 11));
	m_scene.m_chunk.render_wireframe = sqlite3_column_int(pResultsChunk, 12);
	m_scene.m_chunk.render_normals = sqlite3_column_int(pResultsChunk, 13);
	m_scene.m_chunk.tex_diffuse_tiling = sqlite3_column_int(pResultsChunk, 14);
	m_scene.m_chunk.tex_splat_1_tiling = sqlite3_column_int(pResultsChunk, 15);
	m_scene.m_chunk.tex_splat_2_tiling = sqlite3_column_int(pResultsChunk, 16);
	m_scene.m_chunk.tex_splat_3_tiling = sqlite3_column_int(pResultsChunk, 17);
	m_scene.m_chunk.tex_splat_4_tiling = sqlite3_column_int(pResultsChunk, 18);


	//Process Results into renderable
	m_d3dRenderer.BuildDisplayList(&m_scene.m_sceneObjects);
	//build the renderable chunk 
	m_d3dRenderer.BuildDisplayChunk(&m_scene.m_chunk);
	m_d3dRenderer.SetUpCamera(&m_scene);

}

void ToolMain::onActionSave() {
	//SQL
	int rc;
	char *sqlCommand;
	char *ErrMSG = 0;
	sqlite3_stmt *pResults;								//results of the query


	//OBJECTS IN THE WORLD Delete them all
	//prepare SQL Text
	sqlCommand = "DELETE FROM Objects";	 //will delete the whole object table.   Slightly risky but hey.
	rc = sqlite3_prepare_v2(m_databaseConnection, sqlCommand, -1, &pResults, 0);
	sqlite3_step(pResults);

	//Populate with our new objects
	std::wstring sqlCommand2;
	int numObjects = m_scene.m_sceneObjects.size();	//Loop thru the scengraph.

	std::vector<SceneObject>* sceneObjects;

	for (int i = 0; i < numObjects; i++) {
		std::stringstream command;
		command << "INSERT INTO Objects "
			<< "VALUES(" << sceneObjects->at(i).ID << ","
			<< sceneObjects->at(i).chunk_ID << ","
			<< "'" << sceneObjects->at(i).model_path << "'" << ","
			<< "'" << sceneObjects->at(i).tex_diffuse_path << "'" << ","
			<< sceneObjects->at(i).posX << ","
			<< sceneObjects->at(i).posY << ","
			<< sceneObjects->at(i).posZ << ","
			<< sceneObjects->at(i).rotX << ","
			<< sceneObjects->at(i).rotY << ","
			<< sceneObjects->at(i).rotZ << ","
			<< sceneObjects->at(i).scaX << ","
			<< sceneObjects->at(i).scaY << ","
			<< sceneObjects->at(i).scaZ << ","
			<< sceneObjects->at(i).render << ","
			<< sceneObjects->at(i).collision << ","
			<< "'" << sceneObjects->at(i).collision_mesh << "'" << ","
			<< sceneObjects->at(i).collectable << ","
			<< sceneObjects->at(i).destructable << ","
			<< sceneObjects->at(i).health_amount << ","
			<< sceneObjects->at(i).editor_render << ","
			<< sceneObjects->at(i).editor_texture_vis << ","
			<< sceneObjects->at(i).editor_normals_vis << ","
			<< sceneObjects->at(i).editor_collision_vis << ","
			<< sceneObjects->at(i).editor_pivot_vis << ","
			<< sceneObjects->at(i).pivotX << ","
			<< sceneObjects->at(i).pivotY << ","
			<< sceneObjects->at(i).pivotZ << ","
			<< sceneObjects->at(i).snapToGround << ","
			<< sceneObjects->at(i).AINode << ","
			<< "'" << sceneObjects->at(i).audio_path << "'" << ","
			<< sceneObjects->at(i).volume << ","
			<< sceneObjects->at(i).pitch << ","
			<< sceneObjects->at(i).pan << ","
			<< sceneObjects->at(i).one_shot << ","
			<< sceneObjects->at(i).play_on_init << ","
			<< sceneObjects->at(i).play_in_editor << ","
			<< sceneObjects->at(i).min_dist << ","
			<< sceneObjects->at(i).max_dist << ","
			<< sceneObjects->at(i).camera << ","
			<< sceneObjects->at(i).path_node << ","
			<< sceneObjects->at(i).path_node_start << ","
			<< sceneObjects->at(i).path_node_end << ","
			<< sceneObjects->at(i).parent_id << ","
			<< sceneObjects->at(i).editor_wireframe << ","
			<< "'" << sceneObjects->at(i).name << "'"
			<< ")";
		std::string sqlCommand2 = command.str();
		rc = sqlite3_prepare_v2(m_databaseConnection, sqlCommand2.c_str(), -1, &pResults, 0);
		sqlite3_step(pResults);
	}
	MessageBox(NULL, L"Objects Saved", L"Notification", MB_OK);
}

void ToolMain::onActionSaveTerrain() {
	m_d3dRenderer.SaveDisplayChunk(&m_scene.m_chunk);
}

void ToolMain::onActionAddCamera() {

}

void ToolMain::Tick(MSG *msg) {
	//do we have a selection
	//do we have a mode
	//are we clicking / dragging /releasing
	//has something changed
		//update Scenegraph
		//add to scenegraph
		//resend scenegraph to Direct X renderer

	//Renderer Update Call

	if (m_scene.m_cameraChanged) {
		m_d3dRenderer.ChangeCamera(m_scene.m_activeCamera);
		m_scene.m_cameraChanged = false;
	}

	m_d3dRenderer.Tick();
}

void ToolMain::UpdateInput(MSG * msg) {
	switch (msg->message) {
		//Global inputs,  mouse position and keys etc
		case WM_KEYDOWN:
			m_input.SetKeyDown(msg->wParam);
			break;
		case WM_KEYUP:
			m_input.SetKeyUp(msg->wParam);
			break;
		case WM_MOUSEMOVE:
			m_input.UpdateMousePosition(LOWORD(msg->lParam), HIWORD(msg->lParam));
			break;
		case WM_LBUTTONDOWN:
			m_input.SetMouseKeyDown(Input::MouseButton::Left);
			break;
		case WM_LBUTTONUP:
			m_input.SetMouseKeyUp(Input::MouseButton::Left);
			break;
		case WM_MBUTTONDOWN:
			m_input.SetMouseKeyDown(Input::MouseButton::Middle);
			break;
		case WM_MBUTTONUP:
			m_input.SetMouseKeyUp(Input::MouseButton::Middle);
			break;
		case WM_RBUTTONDOWN:
			m_input.SetMouseKeyDown(Input::MouseButton::Right);
			break;
		case WM_RBUTTONUP:
			m_input.SetMouseKeyUp(Input::MouseButton::Right);
			break;
		case WM_VSCROLL:
			if (LOWORD(msg->wParam) == SB_LINEUP) {
				m_input.SetMouseScroll(1);
			} else if (LOWORD(msg->wParam) == SB_LINEDOWN) {
				m_input.SetMouseScroll(-1);
			}
			break;
	}
}
