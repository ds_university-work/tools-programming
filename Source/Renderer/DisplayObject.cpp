#include "DisplayObject.h"

DisplayObject::DisplayObject() {
	m_model = NULL;
	m_texture_diffuse = NULL;
	m_rotation.x = 0.0f;
	m_rotation.y = 0.0f;
	m_rotation.z = 0.0f;
	m_position.x = 0.0f;
	m_position.y = 0.0f;
	m_position.z = 0.0f;
	m_scale.x = 0.0f;
	m_scale.y = 0.0f;
	m_scale.z = 0.0f;
	m_render = true;
	m_wireframe = false;
}


DisplayObject::~DisplayObject() {
	//	delete m_texture_diffuse;
}
