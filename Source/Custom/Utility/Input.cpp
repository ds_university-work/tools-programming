#include "Input.h"

Input::Input() {
}


Input::~Input() {
}

void Input::SetMouseKeyDown(MouseButton button) {
	switch (button) {
		case MouseButton::Left:
			mouse.left = true;
			break;
		case MouseButton::Middle:
			mouse.middle = true;
			break;
		case MouseButton::Right:
			mouse.right = true;
			break;
	}
}

void Input::SetMouseKeyUp(MouseButton button) {
	switch (button) {
		case MouseButton::Left:
			mouse.left = false;
			break;
		case MouseButton::Middle:
			mouse.middle = false;
			break;
		case MouseButton::Right:
			mouse.right = false;
			break;
	}
}

void Input::SetKeyDown(WPARAM key) {
	keys[key] = true;
}

void Input::SetKeyUp(WPARAM key) {
	keys[key] = false;
}

void Input::UpdateMousePosition(int x, int y) {
	mouse.lastPosition = mouse.position;

	mouse.position.x = x;
	mouse.position.y = y;
	
	mouse.moveDistance.x = mouse.lastPosition.x - mouse.position.x;
	mouse.moveDistance.y = mouse.lastPosition.y - mouse.position.y;
}

void Input::SetMouseScroll(int val) {
	mouse.scrollDistance = val;
}

bool Input::IsMouseButtonDown(MouseButton button) {
	switch (button) {
		case MouseButton::Left:
			return mouse.left;
			break;
		case MouseButton::Middle:
			return mouse.middle;
			break;
		case MouseButton::Right:
			return mouse.right;
			break;
	}
}

bool Input::IsKeyDown(int key) {
	return keys[key];
}

void Input::SetMouseX(int x) {
	mouse.position.x = x;
}

void Input::SetMouseY(int y) {
	mouse.position.y = y;
}

int Input::GetMouseX() {
	return mouse.position.x;
}

int Input::GetMouseY() {
	return mouse.position.y;
}

Vector3 Input::GetMouseDistance() {
	return mouse.moveDistance;
}

int Input::GetMouseScroll() {
	return mouse.scrollDistance;
}

void Input::ResetMouse() {
	mouse.moveDistance.x = 0;
	mouse.moveDistance.y = 0;
}
