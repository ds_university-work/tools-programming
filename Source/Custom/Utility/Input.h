#pragma once
#include <d3d11.h>
#include <SimpleMath.h>

using namespace DirectX::SimpleMath;

class Input {
public:
	enum MouseButton { Left, Middle, Right};
	struct Mouse {
		bool left, right, middle;
		Vector2 position;
		Vector2 lastPosition;
		Vector2 moveDistance;
		int scrollDistance;
	};

	Input();
	~Input();

	void SetMouseKeyDown(MouseButton button);
	void SetMouseKeyUp(MouseButton button);
	void SetKeyDown(WPARAM key);
	void SetKeyUp(WPARAM key);
	void UpdateMousePosition(int x, int y);
	void SetMouseScroll(int val);

	bool IsMouseButtonDown(MouseButton button);
	bool IsKeyDown(int key);

	void SetMouseX(int x);
	void SetMouseY(int y);
	int GetMouseX();
	int GetMouseY();

	Vector3 GetMouseDistance();
	int GetMouseScroll();
	void ResetMouse();

private:
	bool keys[512];
	Mouse mouse;
};

