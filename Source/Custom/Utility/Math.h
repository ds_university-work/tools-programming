#pragma once

#define PI 3.1415f
#define RADTODEG(x) (x * 180 / PI)
#define DEGTORAD(x) (x * PI / 180)