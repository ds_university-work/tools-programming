#include "../../stdafx.h"
#include "SceneCameraDialog.h"


IMPLEMENT_DYNAMIC(SceneCameraDialog, CDialogEx)

BEGIN_MESSAGE_MAP(SceneCameraDialog, CDialogEx)
	ON_COMMAND(IDOK, &SceneCameraDialog::End)
	ON_COMMAND(IDC_SELECTCAMERA, &SceneCameraDialog::CameraSelected)
	ON_COMMAND(IDC_ADDCAMERA, &SceneCameraDialog::AddCamera)
END_MESSAGE_MAP()

SceneCameraDialog::SceneCameraDialog(CWnd* pParent, Scene* pScene) : CDialogEx(IDD_DIALOGCAMERA, pParent) {
	m_scene = pScene;
}

SceneCameraDialog::SceneCameraDialog(CWnd* pParent) : CDialogEx(IDD_DIALOGCAMERA, pParent) {
}

SceneCameraDialog::~SceneCameraDialog() {
}

void SceneCameraDialog::InitialiseDialog(Scene* pScene) {
	m_scene = pScene;

	UpdateCameraList();

}
/*void SceneCameraDialog::SetObjectData(ToolMain* pToolSystem) {
	m_sceneObjects = &pToolSystem->m_scene.m_sceneObjects;
	m_currentSelection = &pToolSystem->m_selectedObject;

	UpdateCameraList();

	for (int i = 0; i < m_cameras.size(); i++) {
		std::wstring listBoxEntry = std::to_wstring(m_cameras[i]->ID);
		m_cameraListBox.AddString(listBoxEntry.c_str());
	}
}*/


void SceneCameraDialog::DoDataExchange(CDataExchange* pDX) {
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CAMERALIST, m_cameraListBox);

	DDX_Control(pDX, IDC_POSITIONX, m_position.X);
	DDX_Control(pDX, IDC_POSITIONY, m_position.Y);
	DDX_Control(pDX, IDC_POSITIONZ, m_position.Z);

	DDX_Control(pDX, IDC_ROTATIONX, m_rotation.X);
	DDX_Control(pDX, IDC_ROTATIONY, m_rotation.Y);
	DDX_Control(pDX, IDC_ROTATIONZ, m_rotation.Z);
}

void SceneCameraDialog::End() {
	DestroyWindow();
}

void SceneCameraDialog::CameraSelected() {
	int index = m_cameraListBox.GetCurSel();

	m_scene->m_activeCamera = m_cameras[index];
	m_scene->m_cameraChanged = true;
}

void SceneCameraDialog::AddCamera() {
	m_scene->AddNewSceneObject("Camera", "", "");
	m_scene->m_sceneObjects.back().camera = true;
	
	UpdateCameraList();
}

void SceneCameraDialog::DeleteCamera() {
	m_scene->DeleteSceneObject(m_scene->m_activeCamera->ID);
}

void SceneCameraDialog::SetCameraTransform() {

}

void SceneCameraDialog::SetCameraFront() {
}

void SceneCameraDialog::SetCameraBack() {
}

void SceneCameraDialog::SetCameraTop() {
}

void SceneCameraDialog::SetCameraBottom() {
}

void SceneCameraDialog::SetCameraLeft() {
}

void SceneCameraDialog::SetCameraRight() {
}

void SceneCameraDialog::UpdateCameraList() {
	m_cameras = m_scene->GetCameraSceneObjects();
	m_cameraListBox.ResetContent();

	for (int i = 0; i < m_cameras.size(); i++) {
		std::wstring listBoxEntry;
		listBoxEntry.assign(m_cameras[i]->name.begin(), m_cameras[i]->name.end());
		listBoxEntry.append(L" (" + std::to_wstring(m_cameras[i]->ID) + L")");
		m_cameraListBox.AddString(listBoxEntry.c_str());
	}
}


BOOL SceneCameraDialog::OnInitDialog() {
	CDialogEx::OnInitDialog();

	return TRUE;
}

void SceneCameraDialog::PostNcDestroy() {
}