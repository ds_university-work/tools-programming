#pragma once
#include <afxdialogex.h>
#include "../../Resource Files/resource.h"
#include <afxwin.h>
#include "../../Tool/SceneObject.h"
#include <vector>
#include "../../Tool/ToolMain.h"
#include "../Scene/Scene.h"

struct Vec3EditControl {
	CEditView X;
	CEditView Y;
	CEditView Z;
};

class SceneCameraDialog : public CDialogEx {

	DECLARE_DYNAMIC(SceneCameraDialog)

public:
	SceneCameraDialog(CWnd* pParent, Scene* pScene);
	SceneCameraDialog(CWnd* pParent = NULL);
	virtual ~SceneCameraDialog();

	void InitialiseDialog(Scene * pScene);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	afx_msg void End();
	afx_msg void CameraSelected();
	afx_msg void AddCamera();
	afx_msg void DeleteCamera();
	afx_msg void SetCameraTransform();
	afx_msg void SetCameraFront();
	afx_msg void SetCameraBack();
	afx_msg void SetCameraTop();
	afx_msg void SetCameraBottom();
	afx_msg void SetCameraLeft();
	afx_msg void SetCameraRight();

	void UpdateCameraList();

	Scene* m_scene;
	std::vector<SceneObject*> m_cameras;

	DECLARE_MESSAGE_MAP()

public:
	CListBox m_cameraListBox;
	Vec3EditControl m_position;
	Vec3EditControl m_rotation;

	virtual BOOL OnInitDialog() override;
	virtual void PostNcDestroy();
};

