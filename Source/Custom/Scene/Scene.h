#pragma once
#include <string>
#include <vector>
#include "../../Tool/SceneObject.h"
#include "../../Tool/ChunkObject.h"

class Scene {
public:
	Scene();
	~Scene();

	SceneObject* AddNewSceneObject(std::string name, std::string modelPath, std::string texturePath, float posX = 0, float posY = 0, float posZ = 0, float rotX = 0, float rotY = 0, float rotZ = 0);
	void DeleteSceneObject(int id);

	std::vector<SceneObject*> GetCameraSceneObjects();
	SceneObject* GetSceneObjectFromID(int id);

	std::vector<SceneObject> m_sceneObjects;
	ChunkObject m_chunk;

	bool m_cameraChanged;
	SceneObject* m_activeCamera;

protected:
	int GetFirstUnusedID();

private:


};

