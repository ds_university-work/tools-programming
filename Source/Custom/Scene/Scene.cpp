#include "Scene.h"

Scene::Scene() {
	m_cameraChanged = false;
}

Scene::~Scene() {
}

SceneObject* Scene::AddNewSceneObject(std::string name, std::string modelPath, std::string texturePath, float posX, float posY, float posZ, float rotX, float rotY, float rotZ) {
	m_sceneObjects.push_back(SceneObject());

	SceneObject* so = &m_sceneObjects.back();

	so->name = name;
	so->posX = posX;
	so->posY = posY;
	so->posZ = posZ;
	so->rotX = rotX;
	so->rotY = rotY;
	so->rotZ = rotZ;

	if (modelPath == "") {
		modelPath = "database/data/placeholder.cmo";
	}
	if (texturePath == "") {
		texturePath = "database/data/placeholder.dds";
	}
	so->model_path = modelPath;
	so->tex_diffuse_path = texturePath;
	so->ID = GetFirstUnusedID();

	return so;
}

void Scene::DeleteSceneObject(int id) {
	for (int i = 0; i < m_sceneObjects.size(); i++) {
		if (m_sceneObjects[i].ID == id) {
			m_sceneObjects.erase(m_sceneObjects.begin() + i);
			i--;
		}
	}
}

std::vector<SceneObject*> Scene::GetCameraSceneObjects() {

	std::vector<SceneObject*> cameras;

	for (int i = 0; i < m_sceneObjects.size(); i++) {
		if (m_sceneObjects[i].camera) {
			cameras.push_back(&m_sceneObjects[i]);
		}
	}

	return cameras;
}

SceneObject * Scene::GetSceneObjectFromID(int id) {
	for each (SceneObject so in m_sceneObjects) {
		if (so.ID == id) {
			return &so;
		}
	}

	return nullptr;
}

int Scene::GetFirstUnusedID() {
	for (int i = 0; i < 1024; i++) {
		bool IDused = false;
		for each (SceneObject so in m_sceneObjects) {
			if (so.ID == i) {
				IDused = true;
				break;
			}
		}
		if (!IDused) {
			return i;
		}
	}
	return -1;
}
