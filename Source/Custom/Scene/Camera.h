#pragma once
#include "../../Tool/SceneObject.h"
#include "../Utility//Input.h"
#include <d3d11.h>
#include <SimpleMath.h>

using namespace DirectX::SimpleMath;

class Camera {
public:
	enum Direction {Forward, Back, Left, Right, Up, Down};

	Camera();
	~Camera();

	void Update();
	void ChangeCameraObject(SceneObject* pObject);

	void MoveDirection(Direction d);
	void Rotate(Vector2 distance);

	Vector3 GetPosition();

	Matrix GetWorldMatrix();
	Matrix GetViewMatrix();
	Matrix GetProjectionMatrix();

	void SetFOV(float fov);
	void SetAspectRatio(float aspect);
	void SetNearPlane(float nearPlane);
	void SetFarPlane(float farPlane);

protected:
	void UpdatePosition();
	void UpdateRotation();

private:
	SceneObject* m_cameraObject;

	Vector3 m_position;
	Vector3 m_rotation;
	Vector3 m_lookAt;
	Vector3 m_forward;
	Vector3 m_right;
	Vector3 m_up;

	float m_rotationRate;
	float m_moveSpeed;

	Matrix m_worldMatrix;
	Matrix m_viewMatrix;

	float m_fieldOfView;
	float m_aspectRatio;
	float m_nearPlane;
	float m_farPlane;

};

