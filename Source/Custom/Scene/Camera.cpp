#include "Camera.h"
#include "../Utility/Math.h"

Camera::Camera() {
	m_moveSpeed = 0.2f;
	m_rotationRate = 0.3f;
}


Camera::~Camera() {
}

void Camera::Update() {
	UpdatePosition();
	UpdateRotation();

	float cosX = cos(DEGTORAD(m_rotation.x));
	float cosY = cos(DEGTORAD(m_rotation.y));
	float cosZ = cos(DEGTORAD(m_rotation.z));
	float sinX = sin(DEGTORAD(m_rotation.x));
	float sinY = sin(DEGTORAD(m_rotation.y));
	float sinZ = sin(DEGTORAD(m_rotation.z));

	// Calculate forward vector.
	m_forward.x = sinY * cosX;
	m_forward.y = sinX;
	m_forward.z = cosX * -cosY;
	m_forward.Normalize();

	// Calculate up vector.
	m_up.x = -cosY * sinZ - sinY * sinX * cosZ;
	m_up.y = cosX * cosZ;
	m_up.z = -sinY * sinZ - sinX * cosZ * -cosY;

	// Calculate right vector.
	m_forward.Cross(m_up, m_right);

	// Update lookat point.
	m_lookAt = m_position + m_forward;

	// Apply camera vectors.
	m_viewMatrix = Matrix::CreateLookAt(m_position, m_lookAt, m_up);
}

void Camera::ChangeCameraObject(SceneObject* pObject) {
	m_cameraObject = pObject;
}

void Camera::MoveDirection(Direction d) {
	// Convert the camera object position to Vector3.
	m_position = Vector3(m_cameraObject->posX, m_cameraObject->posY, m_cameraObject->posZ);

	// Translate the camera in the given direction.
	switch (d) {
		case Direction::Forward:
			m_position += m_forward * m_moveSpeed;
			break;
		case Direction::Back:
			m_position -= m_forward * m_moveSpeed;
			break;
		case Direction::Left:
			m_position -= m_right * m_moveSpeed;
			break;
		case Direction::Right:
			m_position += m_right * m_moveSpeed;
			break;
		case Direction::Up:
			m_position += m_up * m_moveSpeed;
			break;
		case Direction::Down:
			m_position -= m_up * m_moveSpeed;
			break;
	}

	// Set the camera object position.
	m_cameraObject->posX = m_position.x;
	m_cameraObject->posY = m_position.y;
	m_cameraObject->posZ = m_position.z;
}

void Camera::Rotate(Vector2 distance) {
	// Rotate the camera through the given axis.
	m_cameraObject->rotX += m_rotationRate * distance.y;
	m_cameraObject->rotY -= m_rotationRate * distance.x;

	UpdateRotation();
}

Vector3 Camera::GetPosition() {
	return m_position;
}

Matrix Camera::GetWorldMatrix() {
	return Matrix::Identity;
}

Matrix Camera::GetViewMatrix() {
	return m_viewMatrix;
}

Matrix Camera::GetProjectionMatrix() {
	Matrix projectionMatrix = Matrix::CreatePerspectiveFieldOfView(
		m_fieldOfView * 3.1415 / 180,
		m_aspectRatio,
		m_nearPlane,
		m_farPlane
	);

	return projectionMatrix;
}

void Camera::SetFOV(float fov) {
	m_fieldOfView = fov;
}

void Camera::SetAspectRatio(float aspect) {
	m_aspectRatio = aspect;
}

void Camera::SetNearPlane(float nearPlane) {
	m_nearPlane = nearPlane;
}

void Camera::SetFarPlane(float farPlane) {
	m_farPlane = farPlane;
}

void Camera::UpdatePosition() {
	m_position = Vector3(m_cameraObject->posX, m_cameraObject->posY, m_cameraObject->posZ);
}

void Camera::UpdateRotation() {
	// Set the camera object rotation.
	m_rotation.x = m_cameraObject->rotX;
	m_rotation.y = m_cameraObject->rotY;
	m_rotation.z = m_cameraObject->rotZ;
}

